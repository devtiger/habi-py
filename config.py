"""[General Configuration Params]
"""
from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))


class Config:

    APP_HOST = environ.get("APP_HOST")
    APP_PORT = environ.get("HOST_PORT")
    MYSQL_HOST = environ.get("MYSQL_HOST")
    MYSQL_PORT = int(environ.get("MYSQL_PORT"))
    MYSQL_USER = environ.get("MYSQL_USER")
    MYSQL_PASSWORD = environ.get("MYSQL_PASSWORD")
    MYSQL_DB = environ.get("MYSQL_DB")
    PIPENV_DONT_LOAD_ENV = environ.get("PIPENV_DONT_LOAD_ENV")
    MYSQL_CURSORCLASS = environ.get('MYSQL_CURSORCLASS')
