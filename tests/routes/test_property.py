from tests import BaseTest


class TestProperty (BaseTest):

    def test_get_properties_200_code(self):
        response = self.client.get('/api/properties')
        self.assertEqual(200, response.status_code)

    def test_get_properties_409_code(self):
        response = self.client.get('/api/properties?status=comprado')
        self.assertEqual(409, response.status_code)
