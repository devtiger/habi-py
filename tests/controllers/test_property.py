from tests import BaseTest

from api.controllers.property import validate_status
from api.controllers.property import purify_request

class TestProperty (BaseTest):

    def test_validate_status_true(self):
        EN_VENTA = "en_venta"
        self.assertTrue(validate_status(EN_VENTA))

    def test_validate_status_false(self):
        COMPRADO = "comprado"
        self.assertFalse(validate_status(COMPRADO))

    def test_purify(self):
        request = "en_venta,vendido"
        result = purify_request(request)
        expect = ["en_venta", "vendido"]
        self.assertEqual(result, expect)
