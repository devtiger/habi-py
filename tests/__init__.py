from unittest import TestCase
from app import create_app


class BaseTest(TestCase):

    def setUp(self):
        self.client = create_app().test_client()
