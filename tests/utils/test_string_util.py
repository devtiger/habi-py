from tests import BaseTest

from api.utils.string_util import StringUtil


class TestStringUtil (BaseTest):

    def test_list_to_group(self):
        group = "(9, 'hola')"
        result = StringUtil.list_to_group([9, "hola"])
        self.assertEqual(group, result)
