## Paquetes que voy a usar:

Flask -> es un framework libiano que simplemente me proporciona
una base de codigo, y la arquitectura queda a mi libre desicion

python-dotenv -> necesario para configurar un entorno basado en 
archivo .env

## Como voy a abordar el problema

Actualmente no tengo acceso a la base de datos,
lo que voy a hacer por el momento es definir una arquitectura
y configurar lo básico

## Arquitectura
Voy a usar una arquitectura básica modelo/vista/controlador, por su facil implementacion
y porque las especificaciones del problema no son complejas, 
ademas de ser una arquitectura usada por muchos frameworks de desarrollo.

## Consideraciones adicionales
Usaré Pipenv, ya que me provee un equilibrio necesario entre un gestor
de paquetes usado en otros frameworks para otros lenguales, como npm, composer o ruby gems.
Y además me permite usar entornos virtuales al estilo de virtualenv para python.

## Ejemplo de consumo:

http://localhost:5000/api/properties?status=vendido&city=pereira,bogota&year=2019,2018

## Ejecucion:

Copiar el archivo .env-example tal cual en un archivo .env
Instalar dependencias

Ejecutar con: `flask run`

Ejecutar test con: `python -m unittest`