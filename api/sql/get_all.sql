SELECT p.id as id, p.address as direccion, p.city as ciudad,
p.price as precio_venta, p.description as descripcion,
s.name as estado, sh.update_date as fecha,
p.year as fecha_construccion
FROM property p
INNER JOIN status_history sh
ON p.id = sh.property_id
INNER JOIN status s
ON sh.status_id = s.id
WHERE s.name in %s
%s
%s
and sh.update_date = (
				SELECT sh2.update_date
				FROM property p2
				INNER JOIN status_history sh2
				ON p2.id = sh2.property_id
				INNER JOIN status s2
				ON sh2.status_id = s2.id
				WHERE p.id = p2.id
				and s2.name = s.name
				ORDER BY sh2.update_date DESC
				LIMIT 1
				)
