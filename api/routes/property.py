from flask import Blueprint

from api.controllers.property import PropertyController

property_api = Blueprint('property_bp', __name__)


@property_api.route('/properties', methods=['GET'])
def get():
    return PropertyController.get()
