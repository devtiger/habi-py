from flask import jsonify, request
from api.repository.implement.connection_impl import ConnectionImpl
from api.repository.implement.property_impl import PropertyImpl

PRE_VENTA_STATUS = "pre_venta"
EN_VENTA_STATUS = "en_venta"
VENDIDO_STATUS = "vendido"


class PropertyController:

    @staticmethod
    def get():
        request_status = request.args.get('status')
        request_city = request.args.get('city')
        request_year = request.args.get('year')
        if request_status:
            status_list = list(request_status.split(","))
            if not validate_status_list(status_list):
                return jsonify({'message': 'Se ha ingresado un estado inválido.'}), 409
        else:
            status_list = [PRE_VENTA_STATUS, EN_VENTA_STATUS, VENDIDO_STATUS]
        cities = purify_request(request_city)
        years = purify_request(request_year)
        connection = ConnectionImpl()
        result = PropertyImpl(connection).get_all(status_list, cities, years)
        return jsonify(result), 200


def validate_status_list(status_list):
    i = 0
    while i < len(status_list):
        if not validate_status(status_list[i]):
            return False
        i += 1
    return True


def validate_status(status):
    if status != PRE_VENTA_STATUS and status != EN_VENTA_STATUS and status != VENDIDO_STATUS:
        return False
    return True


def purify_request(request_dirty):
    if request_dirty:
        return list(request_dirty.split(","))
    return None

