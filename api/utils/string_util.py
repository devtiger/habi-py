class StringUtil:

    @staticmethod
    def list_to_group(elements_list):
        group = "("
        i = 0
        while i < len(elements_list):
            if type(elements_list[i]) == str:
                if i < len(elements_list)-1:
                    group = group + "'" + elements_list[i] + "', "
                else:
                    group = group + "'" + elements_list[i] + "')"
            else:
                if i < len(elements_list)-1:
                    group = group + str(elements_list[i]) + ", "
                else:
                    group = group + str(elements_list[i]) + ")"
            i += 1
        return group
