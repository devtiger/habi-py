import os
from api.repository.i_property import IProperty
from api.repository.implement.connection_impl import ConnectionImpl
from api.utils.string_util import StringUtil

GET_ALL_PATH = (os.path.dirname(__file__)) + "/../../sql/get_all.sql"
CITY_LABEL = "p.city"
YEAR_LABEL = "p.year"


class PropertyImpl (IProperty):

    connection: ConnectionImpl

    def __init__(self, connection):
        self.connection = connection

    def get_all(self, status_list, cities, years):
        return self.__execute_query(GET_ALL_PATH, status_list, cities, years)

    def __execute_query(self, file_path, status_list, cities, years):
        cursor = self.connection.open()
        fd = open(file_path, 'r')
        sql_query = fd.read()
        fd.close()
        sql_query = sql_query % (
            StringUtil.list_to_group(status_list),
            self.__and_where(CITY_LABEL, cities),
            self.__and_where(YEAR_LABEL, years)
        )
        cursor.execute(sql_query)
        data = cursor.fetchall()
        self.connection.close(cursor)
        return data

    def __and_where(self, label, values):
        if values is None:
            return ""
        return " and %s in %s" % (label, StringUtil.list_to_group(values))


