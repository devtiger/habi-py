from api.repository import db
from api.repository.i_connection import IConnection


class ConnectionImpl (IConnection):

    def open(self,):
        return db.connection.cursor()

    def close(self, cursor):
        return cursor.close()
